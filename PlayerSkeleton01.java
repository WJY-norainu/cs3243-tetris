import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

//training first
//output the parameters in a text file
//use the parameters in the text file to create a gene to actually play the game

public class PlayerSkeleton01 {
    Gene gene;

    public PlayerSkeleton01(){
        Select select = new Select(100, 100);
        this.gene = select.getFittestGene();
    }

    //implement this function to have a working system
    public int pickMove(State s, int[][] legalMoves) {
        return this.gene.getBestMove(s);
    }
}
