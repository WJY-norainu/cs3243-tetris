import java.lang.Math;

public class Gene {
    //features used to calculate utility
    //sum of each col's height
    int aggregateHeight;
    //num of holes
    int holes;
    //sum of abs difference in heights of two adj cols
    int bumpiness;
    //lines completed
    int lines;

    //coefficients
    //the initial parameters are hyperparameters, can be changed
    double aggregateHeightCoeff;
    double holesCoeff;
    double bumpinessCoeff;
    double linesCoeff;

    //bestMove calculated by this Gene
    public int bestMove;

    //current state
    State state;

    //other pre-set game settings
    public final static int ROWS = 21;
    public final static int COLS = 10;
    public final static int N_PIECES = 7;

    public Gene(State s) {
        this.state = s;

        this.aggregateHeightCoeff = - Math.random();
        this.holesCoeff = - Math.random();
        this.bumpinessCoeff = - Math.random();
        this.linesCoeff = Math.random();
    }

    //this constructor is used when all parameters are trained
    public Gene(State s, double aggregateHeightCoeff, double holesCoeff, double bumpinessCoeff, double linesCoeff){
        this.state =s ;

        this.aggregateHeightCoeff = aggregateHeightCoeff;
        this.holesCoeff = holesCoeff;
        this.bumpinessCoeff = bumpinessCoeff;
        this.linesCoeff = linesCoeff;
    }

    public int getBestMove(State state) {
        this.state = state;
        int bestMove = 0;
        double maxUtility = getUtility(0);
        for(int i = 1; i < state.legalMoves().length; i++) {
            double utility = getUtility(1);
            if(utility > maxUtility){
                maxUtility = utility;
                bestMove = i;
            }
        }
        return bestMove;
    }

    private double getUtility(int move) {
        int[][] legalMoves = state.legalMoves();
        return makeMove(legalMoves[move][0], legalMoves[move][1]);
    }

    private int getAggregateHeight(int[] top){
        int count = 0;
        for(int i = 0; i < COLS; i++){
            count += top[i];
        }
        return count;
    }

    private int getHoles(int[][] field){
        int count = 0;
        boolean alreadyCounted = false;
        for(int i = 0; i < COLS; i++){
            int colCount = 0;
            for(int j = 0; j <ROWS; j++){
                if(field[i][j] == 0) {
                    colCount++;
                }else {//reached a filled block
                    count += colCount;
                    colCount = 0;
                }
            }
        }
        return count;
    }

    private int getBumpiness(int[] top){
        int sumDifference = 0;
        for(int i = 0; i < COLS - 1; i++){
            sumDifference += Math.abs(top[i] - top[i+1]);
        }
        return sumDifference;
    }

    private int getLines(int[][] field){
        int count = 0;
        for(int j = 0; j < ROWS; j++){
            if(isLine(field[j])){
                count++;
            }
        }
        return count;
    }

    private boolean isLine(int[] row){
        for(int i = 0; i < COLS; i++){
            if(row[i] == 0){
                return false;
            }
        }
        return true;
    }

    //returns false if you lose - true otherwise
    public double makeMove(int orient, int slot) {
        //note top and field should be cloned to avoid modifying the real state
        int[] top = state.getTop().clone();
        int[][] field = state.getField().clone();

        boolean lost = state.lost;
        int nextPiece = state.getNextPiece();
        int turn = state.getTurnNumber();
        int[][] pWidth = state.getpWidth();
        int[][] pHeight = state.getpHeight();
        int[][][] pBottom = state.getpBottom();
        int[][][] pTop = state.getpTop();

        //height if the first column makes contact
        int height = top[slot]-pBottom[nextPiece][orient][0];
        //for each column beyond the first in the piece
        for(int c = 1; c < pWidth[nextPiece][orient];c++) {
            height = Math.max(height,top[slot+c]-pBottom[nextPiece][orient][c]);
        }

        //check if game ended
        if(height+pHeight[nextPiece][orient] >= ROWS) {
            lost = true;
            return Double.NEGATIVE_INFINITY;
        }


        //for each column in the piece - fill in the appropriate blocks
        for(int i = 0; i < pWidth[nextPiece][orient]; i++) {

            //from bottom to top of brick
            for(int h = height+pBottom[nextPiece][orient][i]; h < height+pTop[nextPiece][orient][i]; h++) {
                field[h][i+slot] = turn;
            }
        }

        //adjust top
        for(int c = 0; c < pWidth[nextPiece][orient]; c++) {
            top[slot+c]=height+pTop[nextPiece][orient][c];
        }

        int rowsCleared = 0;

        //check for full rows - starting at the top
        for(int r = height+pHeight[nextPiece][orient]-1; r >= height; r--) {
            //check all columns in the row
            boolean full = true;
            for(int c = 0; c < COLS; c++) {
                if(field[r][c] == 0) {
                    full = false;
                    break;
                }
            }
            //if the row was full - remove it and slide above stuff down
            if(full) {
                rowsCleared++;
                //for each column
                for(int c = 0; c < COLS; c++) {

                    //slide down all bricks
                    for(int i = r; i < top[c]; i++) {
                        field[i][c] = field[i+1][c];
                    }
                    //lower the top
                    top[c]--;
                    while(top[c]>=1 && field[top[c]-1][c]==0)	top[c]--;
                }
            }
        }

        int aggregateHeight = getAggregateHeight(top);
        int holes = getHoles(field);
        int bumpiness = getBumpiness(top);
        int lines = rowsCleared;

        return calc_utility(aggregateHeight, holes, bumpiness, lines);
    }

    private double calc_utility(int aggregateHeight, int holes, int bumpiness, int lines){
        return aggregateHeightCoeff * aggregateHeight
                + holesCoeff * holes
                + bumpinessCoeff * bumpiness
                + linesCoeff * lines;
    }
}
