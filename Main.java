import java.io.File;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Supported modes:");
        System.out.println(" 1. Randomised pieces");
        System.out.println(" 2. Peronalised probability");
        System.out.println(" 3. Sequence defined in file");
        System.out.print("Enter your choice: ");
        int mode = sc.nextInt();
        if(mode == 1){
            PlayerSkeleton ps1 = new PlayerSkeleton();
            ps1.run();
        } else if(mode == 2){
            System.out.print("Enter 7 numbers for probabilities: ");
            double[] probability = new double[7];
            for(int i = 0; i < 7; i++) {
                probability[i] = sc.nextDouble();
            }
            PlayerSkeleton ps2 = new PlayerSkeleton(probability);
            ps2.run();
        } else if(mode == 3){
            File file = new File("input.txt");
            PlayerSkeleton ps3 = new PlayerSkeleton(file);
            ps3.run();
        } else{
            System.err.println("Index out of bound.");
            return;
        }
    }
}
